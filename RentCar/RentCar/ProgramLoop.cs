﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using RentCar.IoHelpers;
using RentCar.BusinessLayer.Dtos;
using RentCar.BusinessLayer.Service;

namespace RentCar
{
    internal class ProgramLoop
    {
        public static int SelectedClient =0;
        public enum CommandTypes
        {
            AddClient,
            AddCar,
            AddCarWithNumberPlate,
            AddRent,
            AddPromotion,
            ReturnCar,
            Exit

        }

        public void Execute()
        {
            var exit = false;
            while (!exit)
            {
                var command = ConsoleReadHelper.GetCommnadType("Wprowadz komende: ");

                switch (command)
                {


                    case CommandTypes.AddCar:
                        AddCar();
                        break;

                    case CommandTypes.AddCarWithNumberPlate:
                        AddCarWithNumberPlate();
                        break;

                    case CommandTypes.AddClient:
                        AddClient();
                        break;

                    case CommandTypes.AddRent:
                       AddRent();
                       break;

                    case CommandTypes.AddPromotion:
                        AddPromotion();
                        break;

                    case CommandTypes.ReturnCar:
                        ReturnCar();
                        break;

                    case CommandTypes.Exit:
                        exit = true;
                        break;



                    default:
                        Console.WriteLine("Command " + command + " is not supported");
                        break;
                }
            }
        }


        private static void ReturnCar()
        {
            try
            {
                var rentService = new RentService();
                var carToRentService = new CarToRentService();

                var clientService = new ClientService();
                var returnList =
                    rentService.GetAllRentsWhereReturnIsFalse(); //wypisuje wszystkie samochody, ktore sa wypozyczone
                foreach (var rent in returnList)
                {
                    Console.Write("Id wypozyczenia " + rent.Id + ", ");
                    var clientList = clientService.GetClientById(rent.ClientId);
                    foreach (var data in clientList) //wypisuje klienta, ktory wypozyczyl
                    {
                        Console.WriteLine("imie klienta " + data.Name + ", ");
                        Console.WriteLine("nazwisko klienta " + data.Surname + ", ");
                        SelectedClient = data.Id;
                    }
                    Console.Write("Id samochodu " + rent.RentCarId + ", ");
                    CarToRentDto carToRentDto =
                        carToRentService.GetCarToRentById(rent
                            .RentCarId); //przypisanie wypozyczenia do konkretnego samochodu
                    carToRentService.GetCarToRentById(rent.RentCarId);
                    Console.Write("rejestracja: " + carToRentDto.NumberPlate + "\n");
                }
                int answer = ConsoleReadHelper.GetNumber("Wpisz Id wypozyczenia: ");
                var returnRent = new ReturnRentDto();
                returnRent.RentId = answer;
                returnRent.ReturnDate = ConsoleReadHelper.GetReturnData();
                var nextRent = rentService.GetRentById(answer); //wyszukuje nr wypozyczenia
                rentService.ChangeAvailble(nextRent.RentCarId, true); //oddanie samochodu tego konkretnego wypozyczenia
                rentService.ChangeReturn(answer,
                    true); //zmieniamy Return w Rent na oddane (w klasie Rent zalozylismy ze Return = false)
                returnRent.PriceToPay =
                    rentService.CalculatePrice(nextRent, returnRent.ReturnDate); //wyliczamy kwote do zaplaty
                Console.WriteLine("Kwota do zaplaty bez promocji: " + returnRent.PriceToPay + " zl.\n");
               
                /*Czy wiecej niz 10 wypozyczen*/
                if (PromotionService.CheckIfClientRentedTenTimes(SelectedClient)) 
                {
                    double discountFive = PromotionService.ClientRentedTenTimes(SelectedClient, returnRent.PriceToPay);
                    Console.WriteLine("Wypozyczyles samochod wiecej niz 10 razy! ");
                    Console.WriteLine("Dostajesz 5 procent promocji! ");
                    Console.Write("Kwota do zapłaty: " + discountFive + "\n\n\n");
                }//sprawdzenie czy klient wypozyczyl wiecej niz 10 razy samochod

                /*Sprawdzanie czy chcemy dodac promocje dla klienta*/
                string answers = ConsoleReadHelper.GetString("-Czy chcesz dodac specjalna promocje (tak/nie)?-\n");

                if (answers == "Tak" || answers == "tak")
                {
                    Console.WriteLine("Dostepne promocje:\n");

                    var promoList = PromotionService.GetAllPromotionWhichAreAvailable();
                    foreach (var promotion in promoList)
                    {
                       Console.WriteLine("Id:"+ promotion.Id + "\t" +
                          promotion.PromotionName + " " + 
                          promotion.PromotionParcent + "%\n");

                    }

                    int id = ConsoleReadHelper.GetNumber("Podaj id promocji: ");
                    int parcent = PromotionService.FindPromotionByIdAndGetParcent(id); //zwraca procent dla id promocji

                    if (PromotionService.CheckIfClientRentedTenTimes(SelectedClient))
                    {
                       double price =   RentCar.BusinessLayer.Service.PromotionService.AddPromotionToPrice(returnRent.PriceToPay, 5+parcent);
                       Console.WriteLine("Cena po zsumowaniu dwoch promocji: " + price + "zl\n");
                    }
                    else
                    {
                        double price = RentCar.BusinessLayer.Service.PromotionService.AddPromotionToPrice(returnRent.PriceToPay, parcent);
                        Console.WriteLine("Cena po odpliczeniu wybranej promocji: " + price + "zl\n");

                    }
                }




                if (rentService.AddReturn(returnRent))
                {
                    Console.WriteLine("Pomyslnie zwrocono samochod. ");
                }
            }
        
            catch (Exception e)
                {
                    Console.WriteLine("Wprowadzono bledne dane. ");
                }
        }

        private static void AddRent()
        {
            try
            {
                bool loop = true;
                while (loop)
                {
                    var rent = new RentDto();
                    
                    rent.RentDate = ConsoleReadHelper.GetRentData();
                    Console.WriteLine("Dostepne modele samochodow: ");
                    var carToRentService = new CarToRentService();
                    var carService = new CarService();

                    foreach (var carToRent in carToRentService.GetAllCarsToRentWhichAreAvailable())
                    {
                        Console.Write("Id: " + carToRent.Id + ", ");
                        foreach (var car in carService.GetCarById(carToRent.CarId))
                        {
                            Console.Write("marka: " + car.BrandName + ", ");
                            Console.Write("model: " + car.Model + ", ");

                        }

                        Console.Write("numer rejestracyjny: " + carToRent.NumberPlate + ", ");
                        if (carToRent.Available)
                            Console.Write("model dostepny");
                        Console.WriteLine("");
                    }
                    string answerExit;
                    answerExit = ConsoleReadHelper.GetAnswer(
                        "Wybierz Id samochodu (jesli chcesz zakonczyc wpisz exit): ");

                    if (answerExit == "exit" || answerExit == "Exit")
                    {
                        loop = false;
                    }
                    else
                    {
                        rent.RentCarId = Int32.Parse(answerExit);
                    }
                    Console.WriteLine("Klienci w bazie: ");
                    var clientService = new ClientService();
                    foreach (var client in clientService.GetAllClients())
                    {
                        Console.Write("Id: " + client.Id + ", ");
                        Console.Write("imie: " + client.Name + ", ");
                        Console.Write("nazwisko: " + client.Surname + ", ");
                        Console.Write("pesel: " + client.Pesel + ". \n");
                    }
                    Console.WriteLine("");
                    string nextAnswerExit;
                    nextAnswerExit = ConsoleReadHelper.GetAnswer(
                        "Wybierz Id klienta (jesli zakonczyc exit): ");

                    if (nextAnswerExit == "exit" || nextAnswerExit == "Exit")
                    {
                        loop = false;
                    }
                    else
                    {
                        rent.ClientId = Int32.Parse(nextAnswerExit);
                        rent.Return = false;
                    }
                    var rentService = new RentService();
                    ;
                    if (rentService.AddRent(rent))
                    {
                        rentService.ChangeAvailble(rent.RentCarId, false);
                        Console.WriteLine("Informacje o wypozyczeniu zostaly dodane do bazy danych. ");
                        return;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        } //dodanie wypozyczenia

        private static void AddCarWithNumberPlate()
        {
            try
            {
                bool loop = true;
                int answerExit;
                while (loop)
                {
                    Console.WriteLine("Dostepne modele samochodow: ");
                    var carService = new CarService();
                    foreach (var list in carService.GetAllCars())
                    {

                        Console.Write("Id: " + list.Id + ", ");
                        Console.Write("nazwa marki: " + list.BrandName + ", ");
                        Console.Write("model: " + list.Model);
                        Console.WriteLine("");
                    }
                    Console.WriteLine("");
                    answerExit = ConsoleReadHelper.GetNumber(
                        "Wybierz Id samochodu, ktoremu chcesz uzupelnic dane. ");
                    {
                        var carWithNumberPlate = new CarToRentDto();
                        carWithNumberPlate.CarId = answerExit;
                        carWithNumberPlate.NumberPlate =
                            ConsoleReadHelper.GetAnswer("Wpisz numer rejestracyjny samochodu;");
                        carWithNumberPlate.Available = true;
                        var carWithNumberPlateService = new CarToRentService();

                        if (carWithNumberPlateService.AddCarToRent(carWithNumberPlate))
                        {
                            Console.WriteLine("Pomyslnie dodano samochod.\n ");
                            loop = false;
                        }
                        else
                        {
                            Console.WriteLine("Nie dodano samochodu. ");

                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        } //dodanie tablicy 
        
        public static void AddCar()
        {
                try
                {

                    int answerExit;
                    bool loop = true;

                    while (loop)
                    {
                        CarDto car = new CarDto();

                        answerExit = ConsoleReadHelper.GetNumber(
                            "Podaj nr porzadkowy samochodu: ");

                        var carService = new CarService();
                        if (carService.CheckIfCarExistById(answerExit))
                        {
                            Console.WriteLine("Podano Id samochodu, ktory juz istnieje. ");
                        }
                        else
                        {
                            car.BrandName = ConsoleReadHelper.GetAnswer("Wpisz marke samochodu: ");
                            car.Model = ConsoleReadHelper.GetAnswer("Wpisz model samochodu: ");
                            car.Price =
                                ConsoleReadHelper.GetAnswerWhichTypeIsDouble(
                                    "Podaj cene samochodu za dobe wynajmu: ");
                            if (carService.AddCar(car))
                            {
                                Console.WriteLine("Pomyslnie dodano samochod. ");
                                loop = false;
                            }
                            else
                            {
                                Console.WriteLine("Nie dodano samochodu. ");
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
        } //dodanie samochodu

        private void AddClient()
            {
                var clientDto = new ClientDto();
                Console.WriteLine("Dane klienta:");

                bool succes;
                do
                {
                    if (succes =
                        ClientService.CheckIfClientPeselExists(clientDto.Pesel = ConsoleReadHelper.GetLong("PESEL: ")))
                    {
                        Console.WriteLine("Podany pesel istnieje w bazie! Spróbuj jeszcze raz..\n");
                    }
                } while (succes);

                clientDto.Name = ConsoleReadHelper.GetString("Imie: ");
                clientDto.Surname = ConsoleReadHelper.GetString("Nazwisko: ");

                var ifClientWasAdded = BusinessLayer.Service.ClientService.AddClient(clientDto);

                if (ifClientWasAdded)
                {
                    //TODO tutaj wykonanie dodania Klienta do bazy, nie wyzej
                    Console.WriteLine("\nKlient zostal dodany do bazy danych.\n");
                }
                else
                {
                    Console.WriteLine("\nNie zostal dodany do bazy danych, poniewaz istnieje juz w bazie.\n");
                }
            } //dodaje klienta do bazy danych

        private void AddPromotion()
            {
                PromotionDto promotionDto = new PromotionDto();
                Console.WriteLine("\n------DodawaniePromocji--------\n");
                promotionDto.PromotionName = ConsoleReadHelper.GetString("Podaj nazwe promocji: ");
                promotionDto.PromotionParcent =
                    ConsoleReadHelper.GetNumber("Podaj procent o jaki ma byc obnizona kwota: ");

                var ifPromotionWasAdded = BusinessLayer.Service.PromotionService.AddPromotion(promotionDto);

            } //dodanie promocji

        

    }
}


