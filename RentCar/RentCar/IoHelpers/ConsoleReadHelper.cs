﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentCar.IoHelpers
{
    class ConsoleReadHelper
    {
        public static string GetAnswer(string message)
        {
            string answer;
            Console.WriteLine(message);
            answer = Console.ReadLine();
            return answer;
        }

        public static double GetAnswerWhichTypeIsDouble(string message)
        {
            double number;
            Console.WriteLine(message);
            while (!Double.TryParse(Console.ReadLine(), out number))
            {
                Console.WriteLine("Nie podano prawidlowej liczby. ");
            }
            return number;
        }

        public static int GetNumber(string message)
        {
            int number;
            Console.WriteLine(message);


            while (!int.TryParse(Console.ReadLine(), out number))
            {
                Console.WriteLine("Nie podano prawidlowej liczby. ");
            }
            return number;
        }

        public static ProgramLoop.CommandTypes GetCommnadType()
        {
            ProgramLoop.CommandTypes commandType;
            Console.WriteLine("|------------------RentCar------------------|\n" +
                              "AddClient - dodanie klienta do bazy" +
                              "\nExit - Wyjscie z aplikacji\n\n");


            while (!Enum.TryParse(Console.ReadLine(), out commandType))
            {
                Console.WriteLine("Wprowadzono nieprawidlowy znak, spróbuj ponownie...\n");

                Console.WriteLine("|------------------DZIENNIK ZAJEĆ------------------|\n" +
                                  "AddClient - dodanie klienta do bazy" +
                                  "\nExit - Wyjscie z aplikacji\n\n");
            }

            return commandType;
        } //Pobiera komende od uzytkownika

        public static long GetLong(string message)
        {
            long number;
            Console.Write(message);

            while (!long.TryParse(Console.ReadLine(), out number))
            {
                Console.Write("Podana liczba jest nieprawidlowego typu, sprobuj ponownie: ");
            }

            return number;
        } //Parsuje na long

        public static string GetString(string message)
        {

            while (true)
            {
                Console.Write(message);
                var variable = Console.ReadLine();
                while (String.IsNullOrEmpty(variable))
                {
                    Console.Write("Nic nie wpisales..\n\n");
                    Console.Write(message);


                    variable = Console.ReadLine();
                }
                return variable;

            }
        }

        public static ProgramLoop.CommandTypes GetCommnadType(string message)
        {
            ProgramLoop.CommandTypes commandType;
            Console.Write(message + " (AddClient, AddCar, AddCarWithNumberPlate, AddRent, ReturnCar, AddPromotion Exit \n) ");

            while (!Enum.TryParse(Console.ReadLine(), out commandType))
            {
                Console.WriteLine("Zostala wybrana nieprowala komenda. Sprobuj jeszcze raz. ");
            }

            return commandType;
        }

        public static DateTime GetRentData()
        {
            string answerDate;
            DateTime date = DateTime.Parse("1999/01/01");

            bool properCourseDate = false;

            while (properCourseDate == false)
            {
                Console.WriteLine("Podaj date wypozyczenia: ");
                answerDate = Console.ReadLine();
                try
                {
                    date = DateTime.Parse(answerDate);
                    properCourseDate = true;

                }
                catch (Exception e)
                {
                    Console.WriteLine("Blednie wprowadzono date.");
                    properCourseDate = false;
                }

            }
            return date;
        }

        public static DateTime GetReturnData()
        {
            string answerDate;
            DateTime date = DateTime.Parse("1999/01/01");

            bool properCourseDate = false;

            while (properCourseDate == false)
            {
                Console.WriteLine("Podaj date zwrotu: ");
                answerDate = Console.ReadLine();
                try
                {
                    date = DateTime.Parse(answerDate);
                    properCourseDate = true;

                }
                catch (Exception e)
                {
                    Console.WriteLine("Blednie wprowadzono date.");
                    properCourseDate = false;
                }

            }
            return date;
        }
    }
}