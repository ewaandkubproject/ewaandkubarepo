﻿using System.Collections.Generic;
using System.Linq;
using RentCar.DataLayer.DbContext;
using RentCar.DataLayer.Models;

namespace RentCar.DataLayer.Repositories
{
    public class CarRepository
    {
        public bool AddCar(Car car)
        {
            int rows = 0;

            using (var dbContex = new RentCarDbContext())
            {

                dbContex.CarDbSet.Add(car);
                rows = dbContex.SaveChanges();
            }

            return (rows != 0);
        }

        public List<Car> GetAllCars()
        {
            List<Car> carsList = null;
            using (var dbContex = new RentCarDbContext())
            {
                carsList = dbContex.CarDbSet.ToList();
            }

            return carsList;
        }

        public List<Car> GetCarById(int Id)
        {
            List<Car> carsList = null;
            {
                using (var dbContex = new RentCarDbContext())
                {
                    carsList = dbContex.CarDbSet.Where(i => i.Id == Id).ToList();
                }
                return carsList;
            }
        }
    }
}
