﻿using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using RentCar.DataLayer.DbContext;
using RentCar.DataLayer.Models;

namespace RentCar.DataLayer.Repositories
{
    public class ClientRepository
    {
        public static bool AddClient(Client client)
        {
            var rowsAffected = 0;
            using (var dbContext = new RentCarDbContext())
            {
                dbContext.ClientDbSet.Add(client);
                rowsAffected = dbContext.SaveChanges();
            }
            return rowsAffected ==1;
        }

        public static bool CheckIfPeselExists(long pesel)
        {
            using (var dbContext = new RentCarDbContext())
            {
                var checkIfPeselExists = dbContext.ClientDbSet.Count(x => x.Pesel == pesel);
                if (checkIfPeselExists != 0)
                {
                    return true; //pesel istnieje juz w bazie
                }
                else
                {
                    return false; //pesel nie istnieje w bazie
                }
            }
        } //czy pesel istnieje w bazie klientow
        public List<Client> GetClientById(int Id)
        {
            List<Client> clientsList = null;
            {
                using (var dbContex = new RentCarDbContext())
                {
                    clientsList = dbContex.ClientDbSet.Where(i => i.Id == Id).ToList();
                }
                return clientsList;
            }
        }
        public List<Client> GetAllClients()
        {
            List<Client> clientsList = null;
            using (var dbContex = new RentCarDbContext())
            {
                clientsList = dbContex.ClientDbSet.ToList();
            }

            return clientsList;
        }


        //public static bool ChangePersonalDataClient(Client client)
        //{
        //    var rowsAffected = 0;
        //    using (var dbContext = new RentCarDbContext())
        //    {
        //        var find = dbContext.ClientDbSet.Find(client.Pesel);
                
        //        rowsAffected = dbContext.SaveChanges();
        //    }
        //    return rowsAffected == 1;
        //}



        //public static bool ChangePersonalDataClient(Client client)
        //{
        //    var rowsAffected = 0;
        //    using (var dbContext = new RentCarDbContext())
        //    {
        //        var find = dbContext.ClientDbSet.Find(client.Pesel);
                
        //        rowsAffected = dbContext.SaveChanges();
        //    }
        //    return rowsAffected == 1;
        //}


        public static bool ChangeClient(Client client)
        {
            int rowsAffected = 0;
            List<Client> clientList = null;

            using (var dbContex = new RentCarDbContext())
            {
                clientList = dbContex.ClientDbSet.Where(a => a.Pesel == client.Pesel).ToList();

                foreach (var clients in clientList)
                {
                    clients.Pesel = client.Pesel;
                    clients.Name = client.Name;
                    clients.Surname = client.Surname;

                }
                rowsAffected = dbContex.SaveChanges();
            }
            return (rowsAffected == 1);
        }

    }
}
