﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RentCar.DataLayer.DbContext;
using RentCar.DataLayer.Models;

namespace RentCar.DataLayer.Repositories
{
    public class CarToRentRepository
    {
        public bool AddCarToRent(CarToRent carToRent)
        {
            int rows = 0;

            using (var dbContex = new RentCarDbContext())
            {

                dbContex.CarToRentDbSet.Add(carToRent);
                rows = dbContex.SaveChanges();
            }

            return (rows != 0);
        }

        public List<CarToRent> GetAllCarsToRentWhichAreAvailable()
        {
            List<CarToRent> carsToRentList = null;
            using (var dbContex = new RentCarDbContext())
            {
                carsToRentList = dbContex.CarToRentDbSet.Where(a => a.Available == true).ToList();
            }
            return carsToRentList;
        }

        public List<CarToRent> GetAllCarsToRentWhichAreNotAvailable()
        {
            List<CarToRent> carsToRentList = null;
            using (var dbContex = new RentCarDbContext())
            {
                carsToRentList = dbContex.CarToRentDbSet.Where(a => a.Available == false).ToList();
            }
            return carsToRentList;
        }

        public List<CarToRent> GetCarToRent(int Id)
        {
            List<CarToRent> carsToRentList = null;
            {
                using (var dbContex = new RentCarDbContext())
                {
                    carsToRentList = dbContex.CarToRentDbSet.Where(i => i.Id == Id).ToList();
                }
                return carsToRentList;
            }

        }
    }
}

