﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RentCar.DataLayer.DbContext;
using RentCar.DataLayer.Models;


namespace RentCar.DataLayer.Repositories
{
   public class PromotionRepository
   {

       /*Zadanie 7*/
       public static bool CheckHowManyRentsForOneId(int clientId)
       {
           int rows = 0;
          
           using (var dbContex = new RentCarDbContext())
           {
               var countRentForOneId = dbContex.RentDbSet.Count(x=>x.ClientId == clientId);
               if (countRentForOneId >= 2)
               {
                   return true; //klient wypozyczl wiecej niz 10 razy samochod
               }
               else
               {
                   return false; //klient wypozyczyl mniej niz 10 razy samochod
               }
           }
           return (rows == 1);
       } //metoda sprawdza ile razy klient wypozyczyl samochod

       /*Zadanie 8*/
       public static bool AddPromotion(Promotion promotion)
       {
           var rowsAffected = 0;
           using (var dbContext = new RentCarDbContext())
           {
               dbContext.PromotionDbSet.Add(promotion);
               rowsAffected = dbContext.SaveChanges();
           }
           return rowsAffected == 1;
       }

       /*Zadanie 9*/
       public List<Promotion> GetAllPromotionsWhichAreAvailable()
       {
           List<Promotion> promotionToList = null;
           using (var dbContex = new RentCarDbContext())
           {
               promotionToList = dbContex.PromotionDbSet.Where(a => a.Id > 0).ToList();
           }
           return promotionToList;
       }

       public static int GetPromotionParcentById(int a)
       {
           int parcent;
           using (var dbContext = new RentCarDbContext())
           {
             var   findPromotion = dbContext.PromotionDbSet.First(x => x.Id == a);
             parcent = findPromotion.PromotionParcent;
           }
           return parcent;
       } //cena po procencie


    }
}
