﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using RentCar.DataLayer.DbContext;
using RentCar.DataLayer.Models;

namespace RentCar.DataLayer.Repositories
{
    public class RentRepository
    {
        public bool AddRent(Rent rent)
        {
            int rows = 0;

            using (var dbContex = new RentCarDbContext())
            {

                dbContex.RentDbSet.Add(rent);
                rows = dbContex.SaveChanges();
            }

            return (rows != 0);
        }
        public bool AddReturn(ReturnRent returnRent)
        {
            int rows = 0;

            using (var dbContex = new RentCarDbContext())
            {

                dbContex.ReturnRentDbSet.Add(returnRent);
                rows = dbContex.SaveChanges();
            }

            return (rows != 0);
        }

        public bool ChangeAvailable(int carToRentId, bool change)
        {
            int rows = 0;
            List<CarToRent> rentList = null;

            using (var dbContex = new RentCarDbContext())
            {
                rentList = dbContex.CarToRentDbSet.Where(a => a.Id == carToRentId).ToList();

                foreach (var car in rentList)
                {
                    car.Available = change;
                }
                rows = dbContex.SaveChanges();
            }
            return (rows == 1);
        }

        public bool ChangeReturn(int rentId, bool change)
        {
            int rows = 0;
            List<Rent> rentList = null;

            using (var dbContex = new RentCarDbContext())
            {
                rentList = dbContex.RentDbSet.Where(r => r.Id == rentId).ToList();

                foreach (var rent in rentList)
                {
                    rent.Return = change;
                }
                rows = dbContex.SaveChanges();
            }
            return (rows == 1);
        }

        public List<Rent> GetClientByCarId(int carId)
        {
            List<Rent> rentList = null;
            {
                using (var dbContex = new RentCarDbContext())
                {
                    rentList = dbContex.RentDbSet.Where(i => i.Id == carId).ToList();
                }
                return rentList;
            }
        }
        public List<Rent> GetAllRentsWhereReturnIsFalse()
        {
            List<Rent> rentsList = null;
            {
                using (var dbContex = new RentCarDbContext())
                {
                    rentsList = dbContex.RentDbSet.Where(r => r.Return == false).ToList();
                }
                return rentsList;
            }
        }

        public List<Rent> GetRentById(int rentId)
        {
            List<Rent> rentsList = null;
            {
                using (var dbContex = new RentCarDbContext())
                {
                    rentsList = dbContex.RentDbSet.Where(r => r.Id==rentId).ToList();
                }
                return rentsList;
            }
        }

    }
}