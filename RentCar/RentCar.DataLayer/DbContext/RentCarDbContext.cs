﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Configuration;
using RentCar.DataLayer.Models;

namespace RentCar.DataLayer.DbContext
{
    internal class RentCarDbContext : System.Data.Entity.DbContext
    {
        public RentCarDbContext() : base(GetConnectionString())
        { }

        public DbSet<Client> ClientDbSet { get; set; }
        public DbSet<Car> CarDbSet { get; set; }
        public DbSet <CarToRent> CarToRentDbSet { get; set; }
        public DbSet<Rent> RentDbSet { get; set; }
        public DbSet<Promotion> PromotionDbSet { get; set; }
        public DbSet<ReturnRent>ReturnRentDbSet { get; set; }


        private static string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["MyDatabase"].ConnectionString;
        }
    }
}
