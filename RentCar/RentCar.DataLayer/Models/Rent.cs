﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentCar.DataLayer.Models
{
   public class Rent
    {
        public DateTime RentDate { get; set; }
        public int Id { get; set; }
        public int ClientId { get; set; }
        public int RentCarId { get; set; }
        public bool Return { get; set; }




    }
}
