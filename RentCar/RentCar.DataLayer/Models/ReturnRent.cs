﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentCar.DataLayer.Models
{
   public class ReturnRent
    {
        public DateTime ReturnDate { get; set; } 
        public double PriceToPay { get; set; }
        public int Id { get; set; }
        public double RentId { get; set; }
    }
}
