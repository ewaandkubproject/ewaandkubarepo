﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentCar.DataLayer.Models
{
    public class CarToRent
    {
        public int Id { get; set; }
        public string NumberPlate { get; set; }
        public bool Available { get; set; }
        public int CarId { get; set; }

    }
}
