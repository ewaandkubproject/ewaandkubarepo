﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentCar.DataLayer.Models
{
     public class Promotion
     {
         public int Id { get; set; }
         public string PromotionName { get; set; }
         public int PromotionParcent { get; set; }
    }
}
