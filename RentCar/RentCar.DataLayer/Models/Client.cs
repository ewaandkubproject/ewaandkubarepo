﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentCar.DataLayer.Models
{
    public class Client
    {
        public int Id { get; set; }
        public long Pesel { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
    }
}
