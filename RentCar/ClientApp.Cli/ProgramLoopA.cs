﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using ClientApp.Cli;
using RentCar.BusinessLayer.Dtos;
using RentCar.IoHelpersA;


namespace ClientApp.Cli
{
    public class ProgramLoopA
    {
        public long SelectedClient = 0; //wybrany klient
        public bool Succes;


        public enum CommandTypes
        {
            LogIn,
            EditPersonalData,
            Exit
        }

        public void Execute()
        {
            var exit = false;
            while (!exit)
            {
                var command = ConsoleReadHelperA.GetCommnadType();

                switch (command)
                {
                    case CommandTypes.LogIn:
                        LogIn();
                        break;

                    case CommandTypes.EditPersonalData:
                        EditPersonalData();
                        break;

                    case CommandTypes.Exit:
                        exit = true;
                        break;

                    default:
                        Console.WriteLine("Komenda" + command + " nie istnieje w bazie");
                        break;
                }
            }
        }


        private bool CheckIfPeselExists()
        {
            var clientDto = new ClientDto();

            do
            {
                Succes = RentCar.BusinessLayer.Service.ClientService
                    .CheckIfClientPeselExists(clientDto.Pesel = ConsoleReadHelperA.GetLong("Podaj Pesel: "));
                if (!Succes)
                {
                    Console.WriteLine("Wprowadzony pesel, nie istnieje w bazie danych. Sprobuj, ponownie..\n");
                }

            } while (!Succes);
            SelectedClient = clientDto.Pesel;

            return true;
        } //metoda do sprawdzania czy pesel istnieje w bazie
        
        private void LogIn()
        {
            
            if (CheckIfPeselExists())
            {
                //drukuj dane, nawet if nie jest potrzebny, bo funckja sama powinna sprawdzić czy podany pesel istnieje
            }



        } //logowanie klienta, aby mógł dostać sie do swoich danych

        private void EditPersonalData()
        {
            Console.WriteLine("Edycja danych personalnych.\n");

            if (CheckIfPeselExists())
            {
                ClientDto clientDto = new ClientDto();
                clientDto.Pesel = SelectedClient;
                clientDto.Name = ConsoleReadHelperA.GetString("Podaj nowe imie: ");
                clientDto.Surname = ConsoleReadHelperA.GetString("Podaj nowe nazwisko: ");
                if (RentCar.BusinessLayer.Service.ClientService.ChangePersonalDataClient(clientDto))
                {
                   Console.WriteLine("Zmieniono dane klienta");
                }
                else
                {
                    Console.WriteLine("Nie zmieniono danych");
                }
                {
                    
                }
                }
        } //medtoda do edycji danych klienta



    }
}
