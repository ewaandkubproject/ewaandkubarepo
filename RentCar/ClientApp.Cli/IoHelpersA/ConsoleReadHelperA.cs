﻿
using System;
using ClientApp.Cli;


namespace RentCar.IoHelpersA
{
    public class ConsoleReadHelperA
    {


        public static int GetNumber(string message)
        {
            int number;
            Console.WriteLine(message);


            while (!int.TryParse(Console.ReadLine(), out number))
            {
                Console.WriteLine("Nie podano prawidlowej liczby. ");
            }
            return number;
        }

        public static ProgramLoopA.CommandTypes GetCommnadType(string message)
        {
            ProgramLoopA.CommandTypes commandType;
            Console.Write(message + " (LogIn, EditPersonalData, Exit) ");

            while (!Enum.TryParse(Console.ReadLine(), out commandType))
            {
                Console.WriteLine("Komenda nie istnieje w bazie...");
            }

            return commandType;
        } //funkcja do wprowadzania komend 

        public static ProgramLoopA.CommandTypes GetCommnadType()
        {
            ProgramLoopA.CommandTypes commandType;
            Console.WriteLine("|------------------Client App------------------|\n\n" +
                              "LogIn - Logowanie" +
                              "\nEditPersonalData - Edycja danych personalnych " +
                              "\nExit - Wyjscie z aplikacji\n\n");

            Console.Write("Wprowadz komende: ");
            while (!Enum.TryParse(Console.ReadLine(), out commandType))
            {
                Console.WriteLine("Wprowadzono nieprawidlowy znak, spróbuj ponownie...\n");

                Console.WriteLine("|------------------Client App------------------|\n\n" +
                                  "LogIn - Logowanie klienta" +
                                  "\nExit - Wyjscie z aplikacji\n\n");
            }

            return commandType;
        } //Wykonuje sie na samym poczatku petli, prosi o wpisanie komendy 

        public static long GetLong(string message)
        {
            long number;
            Console.Write(message);

            while (!long.TryParse(Console.ReadLine(), out number))
            {
                Console.Write("Podana liczba jest nieprawidlowego typu, sprobuj ponownie: ");
            }

            return number;
        } //Parsuje na long

        public static string GetString(string message)
        {

            while (true)
            {
                Console.Write(message);
                var variable = Console.ReadLine();
                while (String.IsNullOrEmpty(variable))
                {
                    Console.Write("Nic nie wpisales..\n\n");
                    Console.Write(message);


                    variable = Console.ReadLine();
                }
                return variable;

            }


        }


    }
}