﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RentCar.BusinessLayer.Dtos;
using RentCar.BusinessLayer.Mappers;
using RentCar.DataLayer.Models;
using RentCar.DataLayer.Repositories;

namespace RentCar.BusinessLayer.Service
{
    public class CarService
    {
        public bool AddCar(CarDto car)
        {
            bool addCar;

            var carRepository = new CarRepository();

            addCar = carRepository.AddCar(DtoToEntityMapper.CarDtoToEntityModel(car));
            return addCar;

        }

        public List<CarDto> GetAllCars()
        {
            CarRepository carRepository = new CarRepository();
            List<CarDto> carsList = EntityToDtoMapper.CarEntityToDto(carRepository.GetAllCars());
            
            return carsList;
        }
        public bool CheckIfCarExistById(int Id)
        {
            bool exist;
            CarRepository carRepository = new CarRepository();
            List<Car> carList = carRepository.GetCarById(Id);

            if (carList == null || carList.Count == 0)
            {
                exist = false;
            }
            else
            {
                exist = true;
            }

            return exist;
        }
       

        public List<CarDto> GetCarById(int Id)
        {
            CarRepository carRepository = new CarRepository();
            List<CarDto> carsList = EntityToDtoMapper.CarEntityToDto(carRepository.GetCarById(Id));
             
            return carsList;
        }

    }
}
