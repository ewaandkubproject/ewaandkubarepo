﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using RentCar.BusinessLayer.Dtos;
using RentCar.BusinessLayer.Mappers;
using RentCar.DataLayer.Models;
using RentCar.DataLayer.Repositories;

namespace RentCar.BusinessLayer.Service
{
    public class RentService
    {
        public bool AddRent(RentDto rent)
        {
            bool addRent;

            var rentRepository = new RentRepository();

            addRent = rentRepository.AddRent(DtoToEntityMapper.RentDtoToEntityModel(rent));
            return addRent;

        }
        public bool AddReturn(ReturnRentDto returnRentDto)
        {
            bool addReturn;

            var rentRepository = new RentRepository();

           addReturn = rentRepository.AddReturn(DtoToEntityMapper.ReturnRentDtoToEntityModel(returnRentDto));
            return addReturn;

        }

        public bool ChangeAvailble(int carId, bool change)
        {
            bool result;
            var rentRepository = new RentRepository();

            result = rentRepository.ChangeAvailable(carId,change);

            return result;
        }
        public bool ChangeReturn(int rentId, bool change)
        {
            bool result;
            var rentRepository = new RentRepository();

            result = rentRepository.ChangeReturn(rentId, change);

            return result;
        }
        public List<RentDto> GetClientByCarId(int carId)
        {
            RentRepository rentRepository=new RentRepository();
            List<RentDto> clientList = EntityToDtoMapper.RentEntityToDto(rentRepository.GetClientByCarId(carId));

            return clientList;
        }
         public List<RentDto> GetAllRentsWhereReturnIsFalse()
        {
            RentRepository rentRepository = new RentRepository();
            List<RentDto>rentsList = EntityToDtoMapper.RentEntityToDto(rentRepository.GetAllRentsWhereReturnIsFalse());

            return rentsList;
        }

        public RentDto GetRentById(int rentId)
        {
            var rentRepository = new RentRepository();

            List<RentDto> rentDtoList = EntityToDtoMapper.RentEntityToDto(rentRepository.GetRentById(rentId));
            return rentDtoList[0];
        }

        public double CalculatePrice(RentDto rentDto, DateTime returnDate)
        {
            var carToRentService = new CarToRentService();
            var carToRentDto = carToRentService.GetCarToRentById(rentDto.RentCarId);//wybieramy cartorent
            var carService = new CarService();
            var carDto = carService.GetCarById(carToRentDto.CarId); //pobieramy cardto - tam jest cena


            return (carDto[0].Price * (returnDate - rentDto.RentDate).TotalDays);//wyliczamy kwote do zaplaty


        }
    }
}
