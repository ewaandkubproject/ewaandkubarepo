﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RentCar.BusinessLayer.Dtos;
using RentCar.BusinessLayer.Mappers;
using RentCar.DataLayer.Models;
using RentCar.DataLayer.Repositories;

namespace RentCar.BusinessLayer.Service
{
    public class ClientService
    {
        public static bool AddClient(ClientDto clientDto)
        {
            if (true)
            {
                var client =
                    Mappers.DtoToEntityMapper.ClientDtoToEntityModel(clientDto); //dodaje clienta do bazy danych
                DataLayer.Repositories.ClientRepository.AddClient(client);
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool CheckIfClientPeselExists(long pesel)
        {
            var checkPesel = RentCar.BusinessLayer.Mappers.DtoToEntityMapper.PeselDtoToEntityModel(pesel);
            if (RentCar.DataLayer.Repositories.ClientRepository.CheckIfPeselExists(checkPesel))
            {
                return true; //istnieje
            }
            else
            {
                return false; //nie istnieje
            }

        }


        public List<ClientDto> GetClientById(int Id)
            {
                ClientRepository clientRepository = new ClientRepository();
                List<ClientDto> clientsList = EntityToDtoMapper.ClientEntityToDto(clientRepository.GetClientById(Id));

                return clientsList;
            }

        public List<ClientDto> GetAllClients()
        {
            ClientRepository clientRepository = new ClientRepository();
            List<ClientDto> clientsList = EntityToDtoMapper.ClientEntityToDto(clientRepository.GetAllClients());

            return clientsList;
        }

        public static bool ChangePersonalDataClient(ClientDto clientDto)
        {
            var client = Mappers.DtoToEntityMapper.ClientDtoToEntityModel(clientDto);
            if (DataLayer.Repositories.ClientRepository.ChangeClient(client))

            {
                return true;//zmieniono dane klienta 
            }
            else
            {
                return false;//zmieniono dane klienta
            }
        }
    }
    }

