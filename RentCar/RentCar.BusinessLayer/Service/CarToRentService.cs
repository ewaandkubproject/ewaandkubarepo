﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RentCar.BusinessLayer.Dtos;
using RentCar.BusinessLayer.Mappers;
using RentCar.DataLayer.Models;
using RentCar.DataLayer.Repositories;

namespace RentCar.BusinessLayer.Service
{
   public class CarToRentService
    {
        public bool AddCarToRent(CarToRentDto carToRentDto)
        {
            bool add;

            var carToRentRepository = new CarToRentRepository();        
            add = carToRentRepository.AddCarToRent(DtoToEntityMapper.CarToRentDtoToEntityModel(carToRentDto));
            return add;

        }

        public List<CarToRentDto> GetAllCarsToRentWhichAreAvailable()
        {
            CarToRentRepository carToRentRepository = new CarToRentRepository();
            List<CarToRentDto> carToRentList = EntityToDtoMapper.CarToRentEntityToDto(carToRentRepository.GetAllCarsToRentWhichAreAvailable());
            return carToRentList;
        }
        public List<CarToRentDto> GetAllCarsToRentWhichAreNotAvailable()
        {
            CarToRentRepository carToRentRepository = new CarToRentRepository();
            List<CarToRentDto> carToRentList = EntityToDtoMapper.CarToRentEntityToDto(carToRentRepository.GetAllCarsToRentWhichAreNotAvailable());
            return carToRentList;
        }
        public bool CheckIfCarToRentExistById(int Id)
        {
            bool exist;
            CarToRentRepository carToRentRepository = new CarToRentRepository();
            List<CarToRent> carList = carToRentRepository.GetCarToRent(Id);

            if (carList == null || carList.Count == 0)
            {
                exist = false;
            }
            else
            {
                exist = true;
            }

            return exist;
        }
        public CarToRentDto GetCarToRentById(int Id)
        {
            
            CarToRentRepository carToRentRepository = new CarToRentRepository();
            List<CarToRentDto> carList = EntityToDtoMapper.CarToRentEntityToDto(carToRentRepository.GetCarToRent(Id));

            return carList[0];
        }


    }
}
