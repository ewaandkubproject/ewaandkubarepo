﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RentCar.BusinessLayer.Dtos;
using RentCar.BusinessLayer.Mappers;
using RentCar.DataLayer.Models;
using RentCar.DataLayer.Repositories;

namespace RentCar.BusinessLayer.Service
{
     public  class PromotionService
     {

        /*Zadanie nr 7*/
        public static double CalculateParcentFromPrice(double priceForRent, int parcent) //Wylicza procent z ceny 
        {
            double parcentFromPriceForRent = priceForRent * parcent / 100;
            return parcentFromPriceForRent; //Procent z ceny
        }

        public static double PriceForRentMinusFiveParcent(double priceForRent) //Wylicza cene o 5% nizsza
        {
            double fiveParcent = CalculateParcentFromPrice(priceForRent, 5);
            return priceForRent - fiveParcent;
        }

        public static double ClientRentedTenTimes(int clientId, double priceForRent)
        {
            double finalPriceForRent;

            if (PromotionRepository.CheckHowManyRentsForOneId(clientId) == true)
            {
                finalPriceForRent = PriceForRentMinusFiveParcent(priceForRent); // zwraca cene o 5% nizsza
            }
            else
            {
                return finalPriceForRent = priceForRent; // zwraca cene bez odliczenia 5%
            }
            return finalPriceForRent;
        } //Sprawdza czy klient wypozyczyl 10 lub wiecej razy i zwraca cene o 5% nizsza

        public static bool CheckIfClientRentedTenTimes(int clientId)
        {
             if (RentCar.DataLayer.Repositories.PromotionRepository.CheckHowManyRentsForOneId(clientId) == true)
             {
                 return true; //wiecej niz 10
             }
             else
             {
                 return false; //mniej niz 10
             }
        }  //sprawdza czy klient wypozyczyl wiecej samochodow

        /*Zadanie 8*/
        public static bool AddPromotion(PromotionDto promotionDto)
         {
             if (true)
             {
                 var promotion =
                     Mappers.DtoToEntityMapper.PromotionDtoToEntityModel(promotionDto); //dodaje clienta do bazy danych

                PromotionRepository.AddPromotion(promotion);
                 return true;
             }
             else
             {
                 return false;
             }
         } //dodawnie promocji okazyjnej

        /*Zadanie 9*/
        public static List<PromotionDto> GetAllPromotionWhichAreAvailable()
         {
             PromotionRepository peromotionRepository = new PromotionRepository();
             List<PromotionDto> promotionToList = EntityToDtoMapper.PromotionEntityToDto(peromotionRepository.GetAllPromotionsWhichAreAvailable());
             return promotionToList;
         } //wyrzuca cala liste promocji

        public static int FindPromotionByIdAndGetParcent(int id)
        {
             int parcent = 0;
             parcent = PromotionRepository.GetPromotionParcentById(id);
             return parcent;
        }

        public static double AddPromotionToPrice(double priceForRent, int parcent)
        {
            double finalPriceForRent=0;
            var parcentFromPromotion = CalculateParcentFromPrice(priceForRent, parcent); //wyliczy wartosc ktora mozna odjac od ceny calkowitej

            finalPriceForRent = priceForRent - parcentFromPromotion;

            return finalPriceForRent;
        } //Podaj cene oraz procent z promocji i wylicz nowa cene


    }
}
