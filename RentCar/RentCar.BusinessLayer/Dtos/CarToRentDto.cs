﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentCar.BusinessLayer.Dtos
{
   public class CarToRentDto
    {
        public int Id;

        public int CarId;
        public string NumberPlate;
        public bool Available;

    }
}
