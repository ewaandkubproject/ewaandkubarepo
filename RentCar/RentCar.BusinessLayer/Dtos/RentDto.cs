﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentCar.BusinessLayer.Dtos
{
   public class RentDto
   {
       public DateTime RentDate;
       public int Id;
       public int ClientId;
       public int RentCarId;
       public bool Return;



   }
}
