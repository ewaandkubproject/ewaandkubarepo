﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentCar.BusinessLayer.Dtos
{
    public class ClientDto
    {
        public int Id;
        public long Pesel;
        public string Name;
        public string Surname;
    }
}
