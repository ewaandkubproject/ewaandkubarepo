﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentCar.BusinessLayer.Dtos
{
   public class CarDto
   {
       public int Id;
       public string BrandName;
       public string Model;
       public double Price;
        
    }
}
