﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentCar.BusinessLayer.Dtos
{
    public class ReturnRentDto
    {
        public int Id;
        public int RentId;
        public DateTime ReturnDate;
        public double PriceToPay;
    }
}
