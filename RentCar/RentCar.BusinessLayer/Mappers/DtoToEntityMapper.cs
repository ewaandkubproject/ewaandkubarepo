﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RentCar.BusinessLayer.Dtos;
using RentCar.DataLayer.Models;

namespace RentCar.BusinessLayer.Mappers
{
    internal class DtoToEntityMapper
    {
        public static CarToRent CarToRentDtoToEntityModel(CarToRentDto carToRentDto)
        {
            CarToRent carToRent = new CarToRent();
            carToRent.Available = carToRentDto.Available;
            carToRent.NumberPlate = carToRentDto.NumberPlate;
            carToRent.Id = carToRentDto.Id;
            carToRent.CarId = carToRentDto.CarId;

            return carToRent;
        }

        public static List<Car> CarDtoToEntityList(List<CarDto> carsDtoList)
        {
            List<Car> carsList = new List<Car>();

            foreach (var carDto in carsList)
            {
                Car car = new Car();
                car.BrandName = carDto.BrandName;
                car.Id = carDto.Id;
                car.Model = carDto.Model;
                car.Price = carDto.Price;
                carsList.Add(car);
            }
            return carsList;
        }

        public static Car CarDtoToEntityModel(CarDto cardto)
        {
            Car car = new Car();
            car.BrandName = cardto.BrandName;
            car.Id = cardto.Id;
            car.Model = cardto.Model;
            car.Price = cardto.Price;

            return car;
        }

        public static Client ClientDtoToEntityModel(ClientDto clientDto)
        {
            var client = new Client();
            client.Id = clientDto.Id;
            client.Pesel = clientDto.Pesel;
            client.Name = clientDto.Name;
            client.Surname = clientDto.Surname;

            return client;
        }

        public static long PeselDtoToEntityModel(long pesel)
        {
            var client = new Client();
            client.Pesel = pesel;

            return client.Pesel;
        }

        public static Rent RentDtoToEntityModel(RentDto rentDto)
        {
            Rent rent = new Rent();
            rent.Id = rentDto.Id;
            rent.RentCarId = rentDto.RentCarId;
            rent.ClientId = rentDto.ClientId;
            rent.RentDate = rentDto.RentDate;
            rent.Return = rentDto.Return;
            return rent;
        }

        public static Promotion PromotionDtoToEntityModel(PromotionDto promotionDto)
        {
            Promotion promotion = new Promotion();

            promotion.PromotionName = promotionDto.PromotionName;
            promotion.PromotionParcent = promotionDto.PromotionParcent;

            return promotion;
        }

        public static ReturnRent ReturnRentDtoToEntityModel(ReturnRentDto returnRentDto)
        {
            ReturnRent returnRent = new ReturnRent();
            returnRent.ReturnDate = returnRentDto.ReturnDate;
            returnRent.PriceToPay = returnRentDto.PriceToPay;
            returnRent.Id = returnRentDto.Id;
            returnRent.RentId = returnRentDto.RentId;

            return returnRent;
        }
    }
}
