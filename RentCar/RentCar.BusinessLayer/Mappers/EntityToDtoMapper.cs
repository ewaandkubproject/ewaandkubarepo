﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RentCar.BusinessLayer.Dtos;
using RentCar.DataLayer.Models;

namespace RentCar.BusinessLayer.Mappers
{
    class EntityToDtoMapper
    {
        public static List<CarDto> CarEntityToDto(List<Car> carsList)
        {
            List<CarDto> carsDtoList = new List<CarDto>();

            foreach (var car in carsList)
            {
                CarDto carDto = new CarDto();
                carDto.BrandName = car.BrandName;
                carDto.Id = car.Id;
                carDto.Model = car.Model;
                carDto.Price = car.Price;
                carsDtoList.Add(carDto);
            }
            return carsDtoList;
        }


        public static CarToRentDto CarToRentEntityToDtoModel(CarToRent carToRent)
        {
            CarToRentDto carToRentDto= new CarToRentDto();

            carToRentDto.Available = carToRent.Available;
            carToRentDto.Id = carToRent.Id;
            carToRentDto.NumberPlate = carToRent.NumberPlate;
            carToRentDto.CarId = carToRent.CarId;
           
            return carToRentDto;
        }

        public static List<CarToRentDto> CarToRentEntityToDto(List<CarToRent> carsToRentList)
        {
            List<CarToRentDto> carsToRentDtoList = new List<CarToRentDto>();

            foreach (var car in carsToRentList)
            {
                CarToRentDto carToRentDto = new CarToRentDto();
                carToRentDto.Id = car.Id;
                carToRentDto.Available = car.Available;
                carToRentDto.CarId = car.CarId;
                carToRentDto.NumberPlate = car.NumberPlate;
                carsToRentDtoList.Add(carToRentDto);
            }
            return carsToRentDtoList;
        }
        public static List<ClientDto> ClientEntityToDto(List<Client> clientList)
        {
            List<ClientDto> clientsDtoList = new List<ClientDto>();

            foreach (var client in clientList)
            {
                ClientDto clientDto = new ClientDto();
                clientDto.Id = client.Id;
                clientDto.Pesel = client.Pesel;
                clientDto.Name = client.Name;
                clientDto.Surname = client.Surname;
                clientsDtoList.Add(clientDto);
            }
            return clientsDtoList;
        }
        public static List<RentDto> RentEntityToDto(List<Rent> rentList)
        {
            List<RentDto> rentDtoList = new List<RentDto>();

            foreach (var rent in rentList)
            {
                RentDto rentDto = new RentDto();
                rentDto.Id = rent.Id;
                rentDto.ClientId = rent.ClientId;
                rentDto.RentCarId = rent.RentCarId;
                rentDto.RentDate = rent.RentDate;
                rentDtoList.Add(rentDto);

            }
            return rentDtoList;
        }

        public static List<PromotionDto> PromotionEntityToDto(List<Promotion> promotionsToList)
        {
            List<PromotionDto> promotionToList = new List<PromotionDto>();

            foreach (var promotion in promotionsToList)
            {
                PromotionDto promotionDto = new PromotionDto();

                promotionDto.Id = promotion.Id;
                promotionDto.PromotionParcent = promotion.PromotionParcent;
                promotionDto.PromotionName = promotion.PromotionName;
                promotionToList.Add(promotionDto);
            }
            return promotionToList;
        }

        //public static List<CarToRentDto> CarToRentEntityToDto(List<CarToRent> carsToRentList)
        //{
        //    List<CarToRentDto> carsToRentDtoList = new List<CarToRentDto>();

        //    foreach (var car in carsToRentList)
        //    {
        //        CarToRentDto carToRentDto = new CarToRentDto();
        //        carToRentDto.Id = car.Id;
        //        carToRentDto.Available = car.Available;
        //        carToRentDto.CarId = car.CarId;
        //        carToRentDto.NumberPlate = car.NumberPlate;
        //        carsToRentDtoList.Add(carToRentDto);
        //    }
        //    return carsToRentDtoList;
        //}
    }
}
